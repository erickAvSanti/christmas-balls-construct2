package com.arquigames.christmasballs.customs;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.arquigames.christmasballs.MainActivity;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * This class echoes a string called from JavaScript.
 */
public class CustomPlugin extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("customFunc")) {
            int code = args.getInt(0);
            this.coolMethod(code, callbackContext);
            return true;
        }
        return false;
    }

    private void coolMethod(int code, CallbackContext callbackContext) {
        if ( code >= 0) {
            //Toast.makeText(this.webView.getContext(),"Test hola",Toast.LENGTH_SHORT).show();
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> ((MainActivity)CustomPlugin.this.webView.getContext()).showAd());

            //Log.e("CUSTOM_PLUGIN","loading ad");
            callbackContext.success(code);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
