cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "com.arquigames.christmasballs.customs.CustomPlugin",
      "file": "plugins/com.arquigames.christmasballs.customs/www/CustomPlugin.js",
      "pluginId": "com.arquigames.christmasballs.customs",
      "clobbers": [
        "cordova.plugins.CustomPlugin"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "com.arquigames.christmasballs.customs": "0.0.1"
  };
});